# -----------------------------------------------------------------------------------------------
# alias 
# -----------------------------------------------------------------------------------------------
alias ls='colorls --sort-dirs '
alias l='colorls --sort-dirs --report -l'
alias ll='colorls --sort-dirs --report -al'
alias vi=nvim
alias vim=nvim
alias v=nvim
alias cat=ccat

# https://denysdovhan.com/spaceship-prompt/docs/Options.html#exit-code-exit_code
SPACESHIP_PROMPT_ADD_NEW_LINE=true
SPACESHIP_PROMPT_DEFAULT_PREFIX="$USER"
SPACESHIP_PROMPT_FIRST_PREFIX_SHOW=true
SPACESHIP_CHAR_SYMBOL="\uf0e7"
SPACESHIP_CHAR_PREFIX="\uf296"
SPACESHIP_CHAR_SUFFIX=(" ")
SPACESHIP_USER_SHOW=true
SPACESHIP_TIME_SHOW=true
SPACESHIP_PYENV_SYMBOL="\ue73c"
SPACESHIP_PYENV_COLOR="green"


# -----------------------------------------------------------------------------------------------
# Environment
# -----------------------------------------------------------------------------------------------
export EDITOR=nvim
export GUI_EDITOR=nvim
export BROWSER=/usr/bin/google-chrome-stable
export PYENV_ROOT=$HOME/.pyenv
export PYENV_PATH=$PYENV_ROOT
if which pyenv > /dev/null; then eval "$(pyenv init -)"; fi
if which pyenv-virtualenv-init > /dev/null; then eval "$(pyenv virtualenv-init -)"; fi

# -----------------------------------------------------------------------------------------------
# vi mode => https://gist.github.com/LukeSmithxyz/e62f26e55ea8b0ed41a65912fbebbe52
# -----------------------------------------------------------------------------------------------
# bindkey -v
# export KEYTIMEOUT=1
# Use vim keys in tab complete menu:
# bindkey -M menuselect 'h' vi-backward-char
# bindkey -M menuselect 'k' vi-up-line-or-history
# bindkey -M menuselect 'l' vi-forward-char
# bindkey -M menuselect 'j' vi-down-line-or-history
# bindkey -v '^?' backward-delete-char
# autoload edit-command-line; zle -N edit-command-line
# bindkey '^e' edit-command-line


# -----------------------------------------------------------------------------------------------
# auto tmux
# -----------------------------------------------------------------------------------------------
# case $- in *i*)
#   [ -z "$TMUX" ] && exec tmux
# esac


# -----------------------------------------------------------------------------------------------
# zsh
# -----------------------------------------------------------------------------------------------
source ~/.oh-my-zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 
source ~/.oh-my-zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh


# -----------------------------------------------------------------------------------------------
# fzf
# -----------------------------------------------------------------------------------------------
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
if [ -f /usr/share/fzf/key-bindings.zsh ];then
    source /usr/share/fzf/key-bindings.zsh
    source /usr/share/fzf/completion.zsh
fi
export ZFZ_DEFAULT_OPTS="--extended"
export FZF_DEFAULT_COMMAND="fd --type f"

neofetch
