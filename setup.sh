#!/bin/bash

PATH="$(pwd)"

# Update Mirror list
sudo echo '## Korea
Server = http://mirror.d-tl.com/manjaro/stable/$repo/$arch

## Taiwan
Server = http://free.nchc.org.tw/manjaro/stable/$repo/$arch

## Rusia
Server = http://mirror.yandex.ru/mirrors/manjaro/stable/$repo/$arch' > /etc/pacman.d/mirrorlist

yay -Syu --noconfirm 
yay -S --noconfirm base-devel
yay -S --noconfirm --nocleanmenu --nodiffmenu \
    google-chrome youtube-dl the_silver_searcher imagewriter polybar-git \
    neovim pyenv pyenv-virtualenv tig yarn ripgrep fd git-delta \
    ttf-d2coding ttf-ubuntu-font-family ruby ruby-colorls neofetch ccat
yay -S --noconfirm --nocleanmenu --nodiffmenu scim scim-hangul 



chsh -s `which zsh`

sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh) --unattended"
sed -i 's/plugins=(git)/plugins=(
  archlinux
  git
  fzf
  history-substring-search
  zsh-autosuggestions
  zsh-syntax-highlighting
)/' ~/.zshrc
sed -i 's|robbyrussell|spaceship|' ~/.zshrc
git clone https://github.com/denysdovhan/spaceship-prompt.git "~/.oh-my-zsh/custom/themes/spaceship-prompt"
ln -s "~/.oh-my-zsh/custom/themes/spaceship-prompt/spaceship.zsh-theme" "~/.oh-my-zsh/custom/themes/spaceship.zsh-theme"

git clone https://github.com/zsh-users/zsh-autosuggestions "~/.oh-my-zsh/plugins/"
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git "~/.oh-my-zsh/plugins/"

# https://www.nerdfonts.com/font-downloads
mkdir -p ~/.local/share/fonts/NerdFonts
curl -o ~/.local/share/fonts/NerdFonts -L https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Meslo.zip
fc-cache

echo "source ${PATH}/zshrc >> ~/.zshrc"
ln -s ${PATH}/tmux.conf ~/.tmux.conf
# http://mayccoll.github.io/Gogh/
# bash -c  "$(wget -qO- https://git.io/vQgMr)"

# Background images
sudo cp ${PATH}/backgrounds/* /usr/share/backgrounds
