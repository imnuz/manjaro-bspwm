yay -Syu --noconfirm 
yay -S --noconfirm --nocleanmenu --nodiffmenu base-devel
yay -S --noconfirm --nocleanmenu --nodiffmenu scim scim-hangul ttf-d2coding ttf-ubuntu-font-family
yay -S --noconfirm --nocleanmenu --nodiffmenu google-chrome youtube-dl the_silver_searcher imagewriter
yay -S --noconfirm --nocleanmenu --nodiffmenu neovim pyenv tig yarn ripgrep 





# A viewer for git and diff output
https://github.com/dandavison/delta#installation
